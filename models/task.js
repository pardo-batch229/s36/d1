const mongoose = require("mongoose");

// blueprint
const taskSchema = new mongoose.Schema({ // new keyword is required because we always expect to get a new data
    name: {
        type: String,
        required: [true, "Task name is required!"]
    },
    status:{
        type: String,
        default: "pending"
    }
});

//exporting
module.exports = mongoose.model("Task", taskSchema); // "Task" can be named anything but need the first letter to be capitalize.
